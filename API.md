# Shifted Blocks API documentation
Here's how you add a shifted block:

First, you always need a block which should be shifted. This block **must**
have at least one texture and it **should** be a full-sized cube. Make
sure this block is registered already.

Then, call `shifted_blocks.register_shifted_block`.

If for some reason you need some extra customization, you can try to use
`minetest.override_item` after the block has been registered. Try to avoid
this.

## `shifted_blocks.register_shifted_block(itemstring, original_block, description, side_texture, help, block_texture, no_craft)`
Registers a shifted block variant of an existing block (which must be
registered *before* this function is called.

The texture on 4 sides will be shifted by a half block length. Two sides will
be the “cross-section” of the block so the block has a phyically consistent
look.

This also registers two crafting recipes of this shape
(unless `no_craft==true`):

    XX

* If `X` is the original block: Yields 2 shifted blocks.
* If `X` is the shifted block: Yields 2 original blocks.

### Parameters

* `itemstring`: Itemstring of the shifted block to register
* `original_block`: Itemstring of the original (unshifted) block
* `description`: Description of the shifted block. Recommended style:
    “Shifted `<name of original block>`”.
* `side_texture`: Optional. If set, this is used as the texture of the
    “open” side of the shifted block. It is recommended to make it look
    like a cross-section of the block.
    Default: Same as the 1st texture of the original block
* `help`: Optional. If set, add a custom help text for this block
    (for `doc_items`). You normally don't need this, there is a sane default.
* `block_texture`: Optional. If set, this is the texture which should be
    “shifted”. Only set this if it really differs from the original.
    Default: 1st texture of the original block.
* `no_craft`: Optional. If `true`, no crafting recipes are registered.
    Default: `false`
* `free_rotation`: Optional: If `true`, rotation by screwdriver is unrestricted.
    Default: Rotation is restricted to 3 axes

### Return value

`nil`.
