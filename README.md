# Shifted Blocks

This mod adds shifted block variants which have their textures offset from their original.

Nice for building. Other mods can use an API to add more shifted blocks (see `API.md`).

This mod has no hard dependencies.

## List of blocks

If you use Minetest Game, the following shifted blocks are added:

* Shifted Stone Block
* Shifted Desert Stone Block
* Shifted Sandstone Stone Block
* Shifted Desert Sandstone Block
* Shifted Silver Sandstone Block
* Shifted Obsidian Block
* Shifted Gold Block
* Shifted Bronze Block
* Shifted Copper Block
* Shifted Tin Block
* Shifted Steel Block

If you do not use Minetest Game, shifted blocks may still be added by other mods.

## Crafting

To get 2 shifted blocks:

    XX

To get 2 original blocks:

    SS

* `X` = original block
* `S` = shifted block

## Known bugs

* This mod may have problems with textures other than 16×16.
* Rotation with screendriver may look ugly
* Initial rotation may look ugly as well (because unrestricted)

## License

WTFPL.
