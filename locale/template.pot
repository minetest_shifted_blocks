# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-08 15:28+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: init.lua
msgid ""
"A shifted block is a variant of a block which has been visually moved by a "
"half block length."
msgstr ""

#: init.lua
msgid "Shifted Stone Block"
msgstr ""

#: init.lua
msgid "Shifted Desert Stone Block"
msgstr ""

#: init.lua
msgid "Shifted Sandstone Block"
msgstr ""

#: init.lua
msgid "Shifted Desert Sandstone Block"
msgstr ""

#: init.lua
msgid "Shifted Silver Sandstone Block"
msgstr ""

#: init.lua
msgid "Shifted Obsidian Block"
msgstr ""

#: init.lua
msgid "Shifted Gold Block"
msgstr ""

#: init.lua
msgid "Shifted Bronze Block"
msgstr ""

#: init.lua
msgid "Shifted Tin Block"
msgstr ""

#: init.lua
msgid "Shifted Copper Block"
msgstr ""

#: init.lua
msgid "Shifted Steel Block"
msgstr ""
