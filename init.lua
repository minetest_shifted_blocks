-- Load support for intllib.
local MP = minetest.get_modpath(minetest.get_current_modname())
local S, NS = dofile(MP.."/intllib.lua")

-- doc_items support
local help_default
if minetest.get_modpath("doc_items") then
	help_default = S("A shifted block is a variant of a block which has been visually moved by a half block length.")
end

shifted_blocks = {}

local on_rotate
if minetest.get_modpath("screwdriver") then
	on_rotate = function(pos, node, user, mode, new_param2)
		if mode == screwdriver.ROTATE_FACE then
			if node.param2 == 0 then
				node.param2 = 3
			elseif node.param2 ~= 14 then
				node.param2 = 0
			end
			minetest.set_node(pos, node)
			return true
		elseif mode == screwdriver.ROTATE_AXIS then
			if node.param2 == 0 or node.param2 == 3 then
				node.param2 = 14
			else
				node.param2 = 0
			end
			minetest.set_node(pos, node)
			return true
		else
			return false
		end
	end
end

-- Shifted blocks

shifted_blocks.register_shifted_block = function(itemstring, original_block, description, side_texture, help, block_texture, no_craft, free_rotation)
	local stone = side_texture or minetest.registered_nodes[original_block].tiles[1]
	local normal = block_texture or minetest.registered_nodes[original_block].tiles[1]

	-- FIXME/TEST: Does this work with textures other than 16×16?
	local combine = "[combine:16x16:-8,0="..normal..":8,0="..normal

	if minetest.get_modpath("doc_items") then
		if help == nil then
			help = help_default
		end
	else
		help = nil
	end

	local rotate = on_rotate
	local place_param2 = 0
	if free_rotation then
		rotate = nil
		place_param2 = nil
	end

	minetest.register_node(itemstring, {
		description = description,
		_doc_items_longdesc = help,
		on_rotate = rotate,
		paramtype2 = "facedir",
		-- TODO: Place node according to view direction (restrict param2 to 0 or 3)
		place_param2 = place_param2,
		tiles = {combine, combine, stone, stone, combine.."^[transformFX", combine},
		is_ground_content = false,
		groups = minetest.registered_nodes[original_block].groups,
		sounds = minetest.registered_nodes[original_block].sounds,
	})

	if not no_craft then
		-- Recipes to add shifted block
		minetest.register_craft({
			output = itemstring.." 2",
			recipe = { { original_block, original_block } },
		})
		-- Reverse recipe
		minetest.register_craft({
			output = original_block.." 2",
			recipe = { { itemstring, itemstring} },
		})
	end
end


-- Register shifted blocks for Minetest Game

if minetest.get_modpath("default") then
	-- Stone blocks
	shifted_blocks.register_shifted_block("shifted_blocks:stone_block_shifted", "default:stone_block", S("Shifted Stone Block"), "default_stone.png")
	shifted_blocks.register_shifted_block("shifted_blocks:desert_stone_block_shifted", "default:desert_stone_block", S("Shifted Desert Stone Block"), "default_desert_stone.png")
	shifted_blocks.register_shifted_block("shifted_blocks:sandstone_block_shifted", "default:sandstone_block", S("Shifted Sandstone Block"), "default_sandstone.png")
	shifted_blocks.register_shifted_block("shifted_blocks:desert_sandstone_block_shifted", "default:desert_sandstone_block", S("Shifted Desert Sandstone Block"), "default_desert_sandstone.png")
	shifted_blocks.register_shifted_block("shifted_blocks:silver_sandstone_block_shifted", "default:silver_sandstone_block", S("Shifted Silver Sandstone Block"), "default_silver_sandstone.png")
	shifted_blocks.register_shifted_block("shifted_blocks:obsidian_block_shifted", "default:obsidian_block", S("Shifted Obsidian Block"), "default_obsidian.png")

	-- Metal blocks
	shifted_blocks.register_shifted_block("shifted_blocks:gold_block_shifted", "default:goldblock", S("Shifted Gold Block"))
	shifted_blocks.register_shifted_block("shifted_blocks:bronze_block_shifted", "default:bronzeblock", S("Shifted Bronze Block"))
	shifted_blocks.register_shifted_block("shifted_blocks:tin_block_shifted", "default:tinblock", S("Shifted Tin Block"))
	shifted_blocks.register_shifted_block("shifted_blocks:copper_block_shifted", "default:copperblock", S("Shifted Copper Block"))
	shifted_blocks.register_shifted_block("shifted_blocks:steel_block_shifted", "default:steelblock", S("Shifted Steel Block"))
end
